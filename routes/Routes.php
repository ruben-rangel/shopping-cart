<?php

use App\controllers\ControllerProducts;
use App\controllers\Route;

// This is the index page. The first route.
Route::set('index.php', function () {
	Route::make('layout', []);
});
/* Products rating */
Route::set('rating', function () {
	Route::make('rating', $_POST);
});
// This is a cart route
Route::set('cart', function () {
	Route::make('cart',$_POST);
});

// This is a route, to checkout.
Route::set('checkout',  function () {
	Route::make('checkout',$_POST);
});
// This is a route, to placeorder.
Route::set('placeorder',  function () {
	Route::make('placeorder',$_POST);
});
// This is a route, to login.
Route::set('login',  function () {
	Route::make('login',$_POST);
});
//logout
Route::set('logout',  function () {
	Route::make('logout',$_POST);
});
/* This is a route, to dashboard. */
Route::set('dashboard',  function () {
	Route::make('dashboard',$_POST);
});
//Take Order
Route::set('payment', function () {
  Route::make('payment',$_POST);
});
//Update Data User
Route::set('upuser', function () {
  Route::make('upuser',$_POST);
});

/*
 * This is an example of a dynamic route. In this example,
 * this route would display a users profile page.
*/
Route::set('user/<1>', function () {
  Controller::make('UserProfile');
});
