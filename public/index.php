<?php

use App\controllers\Route;

require_once "../config/basics.php";
require_once "../vendor/autoload.php";
require_once "../routes/Routes.php";

// We create a new instance of the 'ShoppingCart' object and execute the run method.
$shopping = new Route();
$shopping->run();
