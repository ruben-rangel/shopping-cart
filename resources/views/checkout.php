<div class="col-md-12">
	<h1 class="text-center mx-auto">CheckOut</h1>
	<?php
		foreach($this->auxData as $key => $value)
		{
			echo "<input type='hidden' name='{$key}' value='{$value}'>";
		}
	?>
</div>
		<?php
		$products = $this->products;
		if ( count($products) === 0 )
		{
		?>
			<div class="col-md-12 text-center m-5 mx-auto alert alert-danger"><h2>You do not select product(s)</h2></div>
		<?php
		} else
		{
		?>
		<div class="col-md-12 text-center">
			<form action="placeorder" class="p-5" method="POST">
				<div class="row">
					<?php
					$total = 0;
					
					/* Display products to Checkout */
					foreach ($products as $product) 
					{
						$total+= $product['price'];
					
						foreach($this->arrayKeyCuantity as $key => $value)
						{
						$idAux = explode("-", $key);
						$idAux = $idAux[1];
							if ( $product["id"] == $idAux )
							{
								$quantityAux = $value;
							}
						}	
					?>
					<div class="col-md-8 text-left p-2">
						<h4><?php echo ucfirst($product['name']); ?></h4>
						<?php echo  ($quantityAux > 1 ? $quantityAux." items" : $quantityAux." item"); ?>
						<input type="hidden" name="productid-<?php echo $product["id"]; ?>" value="<?php echo $product["id"]; ?>">
						<input type="hidden" name="name-<?php echo $product["id"]; ?>" value="<?php echo $product['name']; ?>">
						<input type="hidden" name="quantity-<?php echo $product["id"]; ?>" value="<?php echo $quantityAux; ?>">
					</div>
					<div class="col-md-4 text-left p-3">
						&#36; <?php echo number_format( $product['price'], 2, '.', ',' ); ?>
						<input type="hidden" value="<?php echo $product["price"]; ?>" name="price-<?php echo $product["id"]; ?>" >
						
					</div>
					<div class="col-md-12"><hr></div>
					<?php
						} //end foreach
					?>
					<input type="hidden" value="<?php echo $this->total; ?>" name="total" >
					<div class="col-md-8">&nbsp;</div>
					<div class="col-md-4">
						<h4>Total <?php echo "({$products->count()})"; ?> Items</h4>
						<h4>&#36; <?php echo number_format($this->total, 2, '.', ',' ); ?></h4>
						<input type="submit" value="Place Order" class="btn btn-dark">
					</div>
				</div>
			</form>
		</div><!--/.col-md-12-->	
		<?php
		} //end else
		?>