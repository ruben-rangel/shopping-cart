<div class="col-md-12">
	<h1 class="text-center mx-auto">Shopping Cart</h1>
</div>	

	<div class="row">
		<?php
		$products = $this->products;
		if (empty($products))
		{
		?>
			<div class="col-md-12 text-center m-5 mx-auto alert alert-danger"><h2>You do not select product(s)</h2></div>
		<?php
		} else
		{
		?>
			<form action="cart" method="POST" id="ProductForm">
			<div class="row">
			
		  <?php foreach ($products as $product): ?>
				<div class="col-md-3 col-sm-1 p-2">
					<div class="card border-0" style="width: 18rem;">
					  <img src="../storage/product-images/<?php echo $product["image"]; ?>" class="card-img-top" alt="..." style="width:288px; height:192px;">
					  <div class="card-body">
						<h5 class="card-title"><?php echo ucfirst($product["name"]); ?></h5>
						<p class="card-text"><?php echo $product["description"]; ?></p>
						<p><strong>&#36; <?php echo number_format( $product["price"], 2, '.', ',' ); ?></strong></p>
						<div class="mt-2 mb-2 prodrating" data-prodrat="<?php echo $product["id"]; ?>">
							<?php for($i= 1; $i <= 5; $i++): ;?>
							<a href="#" data-rating="<?php echo $i; ?>">
								<i class="fas fa-star" style="color:<?php echo ( $i <= $product["rating"]  ? "#007bff" : "" ); ?>;"></i>
							</a>
							
							<?php endfor; ?>
							<p>Rating: <span id="num-rating-<?php echo $product["id"]; ?>"><?php echo !is_null($product["rating"]) ? $product["rating"] : 0 ; ?></span></p>
						</div>
						<a href="#" class="btn btn-smal btn-primary btn-sm btn-block" data-id="<?php echo $product["id"]; ?>">Add to Cart</a>
						
					  </div>
					</div>
				</div>
			<?php endforeach; ?>
      <div class="col-md-12">
        <a href="cart" class="link-cart" style="color:#fff"><img src="../storage/product-images/favicon.jpg" height="50px" width="50px"></a>
        <span class="badge badge-dark link-cart">
          <a href="cart" id="link-cart" class="showNumP" style="color:#fff">
            <?php 
              echo (!empty($this->countCartProducts) ? $this->countCartProducts : "0") 
            ?>
          </a> 
        </span>
      </div>
			<input type="hidden" name="prodSel" id="prodSel" value="0">
			<input type="hidden" name="email" id="session-email" value="<?php echo !empty($this->email) ? $this->email : ""; ?>">
			<input type="hidden" name="session_id" id="session_id" value="<?php echo !empty($this->session_id) ? $this->session_id : ""; ?>">
			</div>
			</form>
		<?php	
		} // end else
		?>
	</div>
	
<!-- Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rating</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        You are not logged In. Please <a href="/shopping-cart/public/login">Sign In...</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<script>
  /* Array to Counts a controller products in Cart */
  var arrayProduct = [];
  /* When click in Add to Cart get Id of Product */
  $('.btn-block').on('click',function(e){
    e.preventDefault();
    let idproduct = $(this).data('id');
    if (!arrayProduct.includes(idproduct))
    {
      arrayProduct.push(idproduct);
      $('#prodSel').val(arrayProduct) ;
      $(this).removeClass('btn-primary').addClass('btn-success');
      $(this).text('Update Cart')
    }
    $('.showNumP').text(arrayProduct.length);
  });

  $('.link-cart').on('click',function(e){
    e.preventDefault();
	if ( $( '#prodSel' ).val() <= 0 )
	{
		$('#alertModal h5.modal-title').text('Products');
		$('#alertModal .modal-body').text('You not selected products');
		$('#alertModal').modal('show');
		return false;
	}	
    console.info('into link-cart');
    $('#ProductForm').submit();
  });
  
$('.fa-star').on('click', function(e){
	e.preventDefault();
	/* If email is Empty the user not login and not vote rating*/
	if ( $('#session-email').val() == ''  )
	{
		$('#alertModal').modal('show');
		return false;
	}
	
	if ( $('#session_id').val() !== '' )
	{
		$('#alertModal .modal-body').text('Only One vote per Session. Thanks!');
		$('#alertModal').modal('show');
		return false;
	}
		
	
	let product_id = parseInt($(this).parents().eq(1).attr('data-prodrat'));
	let rating = parseInt($(this).parent().data('rating'));
	
	data = {'product_id':product_id,'rating':rating};
	$.ajax({
		type: "POST",
		url: "rating",
		dataType: 'json',
		data: data,
		success: function(resp){
			console.info(resp);
			if ( resp.flag === 0 )
			{
				$('#alertModal .modal-body').text( resp.msg );
				$('#alertModal').modal('show');
				return false;
			}
			if ( resp.flag === 1 )
			{
				$('#alertModal .modal-body').text( resp.msg );
				$('#alertModal').modal('show');
			}
			
			/*Put the session_id into value hidden*/
			$('#session_id').val(resp.session_id);
			/*Clear all star and set the new rating*/
			$( 'div[data-prodrat=' + resp.product_id + '] a[data-rating] i.fas.fa-star').css({'color':'#c3bdbd'});
			/* Set rating whit the new */
			for (let i = 1; i <= resp.rating; i++ )
			{
				$( 'div[data-prodrat=' + resp.product_id + '] a[data-rating='+i+'] i.fas.fa-star').css({'color':'red'});
			}
			/*Set de Rating into <span>*/
			$('#num-rating-'+resp.product_id).text(resp.rating);
			
			
			
		},
		error: function(req){
			console.log('An error occurred.');
		}
	});
	
	
});
</script>

