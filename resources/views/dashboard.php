<div class="container">
    
    <div class="row">
		
			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
					<a class="nav-item nav-link " id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Orders( <?php echo !empty(isset($this->ordersUser)) ? count($this->ordersUser) : 0 ?> )</a>
					<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">My Info</a>
				</div>
			</nav>
	</div>	
		
		
		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade show  active p-5" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
				
				<?php
            if ( !isset($this->countCartProducts) )
            {
          ?>
				<div class="row m-5">
				  <div class="col-md-12">
					<div class="col-md-12 mb-2 text-center mx-auto" >
						<a href="/shopping-cart/public"><i class="fas fa-cart-arrow-down" style="color: #000;font-size:2rem;color: Dodgerblue;"></i></a>
					</div>
					<div class="alert alert-primary" role="alert">
					  <?php echo $this->title_message;  ?>
					</div>
				  </div>
				</div>
				<?php
            } 	//title message
            ?>

				
				
				<div class="row mt-5">
					<div class="col-md-12 alert alert-success" style="display:inline;">
						<h5>Available</h5>
						<h3 style="color: #007bff;display: inline;">&#36;  <?php echo ( !empty($this->customer["amount"]) ? number_format( $this->customer["amount"],"2",".","," ) : "0.00" ) ; ?></h3>
						<h3 style="color: #155724;display: inline;">   <?php echo  ( !empty( $this->auxData["total"] ) ? "- &#36; ". number_format( $this->auxData["total"],"2",".","," )  : "" ); ?></h3>
						<h3 style="display: inline;">   <?php echo !empty( $this->auxData["total"] ) ? "= &#36; ". number_format( ($this->customer["amount"] - $this->auxData["total"]),"2",".","," ) : "" ; ?></h3>
					</div>
				</div>	
				
				
				<?php
					if( !empty($this->countCartProducts) )
					{	//have order
				?>
					
					<div class="row mt-5">
					<div class="col-md-12 p-4">
						<h2 class="mute">
							Take your Order | User ID: <?php echo isset($this->id) ? $this->id : ""; ?>
						</h2>
					</div>
						
					<div class="col-md-4 mb-3"><h3>Product</h3></div>
					<div class="col-md-4 mb-3"><h3>Quantity</h3></div>
					<div class="col-md-4 mb-3"><h3>Price( &#36; )</h3></div>
						
					<?php
						if ( !empty($this->auxData) )
						{			
						foreach( $this->auxData as $key => $value )
						{
                //var_dump(substr($key, 0, 9 ));
                if ( 
                  substr($key, 0, 10 ) == "productid-" || 
                  substr($key, 0, 5 ) == "total"  ||
                  substr($key, 0, 5 ) == "email"  ||
                  substr($key, 0, 8 ) == "password"  
                )
                {
                  continue;
                }
                
              ?>
					<div class="col-md-4">
						<h6><?php echo ucfirst($value); ?></h6>
					</div>
                
					<?php
              } //end foreach
					}//end if  
              ?>
			</div><!--/.row-->
					<div class="row">
						<div class="col-md-12">
							<form method="POST" action="payment" id="formpay">
								<div class="col-md-12 text-right mt-4 mr-5" style="margin-top: 1px #000 solid;">
									<div class="form-check form-check-inline">
									  <input class="form-check-input" type="radio"  name="transport" id="inlineRadio1" value="1">
									  <label class="form-check-label" for="inlineRadio1"><strong>Pick-UP &#36; 0</strong></label>
									</div>
									<div class="form-check form-check-inline">
									  <input class="form-check-input" type="radio" name="transport" id="inlineRadio2" value="2">
									  <label class="form-check-label" for="inlineRadio2"><strong>UPS &#36; 5</strong></label>
									</div>
								</div>
								<div class="col-md-12 text-right mt-4 mr-5" style="margin-top: 1px #000 solid;">
									<h3>Total &#36; <?php echo isset( $this->auxData["total"] ) ?  number_format($this->auxData["total"],"2",".",",") : ""; ?></h3>
								</div>
								
								<?php
									//var_dump($this->ids);
									if( !empty($this->ids) )
									{ 
										foreach( $this->ids as $key => $value )
										{
								?>
                    						<input type="hidden" name="id[]" value="<?= $value; ?>"/>
									<?php
									  // var_dump($value);
									  //$id = $value;
                      
										} //end foreach

										foreach( $this->names as $key => $value )
										{
									?>
											<input type="hidden" name="<?= $key;?>" value="<?= $value; ?>"/>
									<?php									
										}	//end foreach
								
										foreach( $this->prices as $key => $value )
										{
										?>
											<input type="hidden" name="<?= $key;?>" value="<?= $value; ?>"/>
										<?php									
                    
										}
								
										foreach( $this->quantity as $key => $value )
										{
										?>
											<input type="hidden" name="<?= $key;?>" value="<?= $value; ?>"/>
										<?php		
										}
										}	// end if ID's
										?>
										<input type="hidden" name="total" value="<?php echo isset($this->auxData["total"]) ?  $this->auxData["total"] : "";?>">
										<input type="hidden" name="idUser" value="<?php echo isset($this->id) ? $this->id : ""; ?>">
										<div class="col-md-12 text-center mt-4 mr-5" style="margin-top: 1px #000 solid;">
											<button type="submit" class="btn btn-primary">Take Order </button>
										</div>
							</form>	
						</div>	
					</div>
					
					
					
					
					
					
					
					
					
					
					
				<?php
					}	//have order
				?>
				
				
				
			  
			</div><!--/.show.active-->
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			

			<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
				<div class="row m-5"> 
					<div class="col-md-12 tex-center mx-auto"> 
						<h3 class="mx-auto">
							Orders 
							| User ID: <?php echo $this->id;?>
						</h3>
					</div>
					<div class="col-md-12 tex-center"> 
						<table class="mx-auto table table-striped">
							<thead >
								<th>Order</th>
								<th class="text-center">Date</th>
								<th class="text-center">Status</th>
								<th>Total</th>
							</thead>
							<tbody>
								<?php
								if (!empty($this->ordersUser))
									{
										foreach( $this->ordersUser as $order )
										{
											$tr = "<tr>";		//init tr

											$tr .= "<td class='text-center'>";
											$tr .= $order["id"];
											$tr .= "</td>";

											$tr .= "<td class='text-center'>";
											$tr .= date('m-d-Y', strtotime($order["created_at"]));
											$tr .= "</td>";
											
											$tr .= "<td class='text-center'>";
											$tr .= $order["status"];
											$tr .= "</td>";
											
											$tr .= "<td class='text-right'>";
											$tr .= $order["total"]." &#036;";
											$tr .= "</td>";

											$tr .= "</tr>";	//end tr
											
											echo $tr;
										}
									}
								
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>  
			
			
			<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
				<div class="row mt-5">
					<div class="col-md-12 mx-auto">
							<form action="upuser" method="POST" id="dataCustomer">
								<div class="lds-facebook" style="display:none;">
									<div></div>
									<div></div>
									<div></div>
								</div>
								<div class="col-md-12 content-form-customer">
									<div class="form-group row">
										<label for="firstName" class="col-sm-2 col-md-2 col-form-label text-right">First Name</label>
										<div class="col-sm-10 col-md-10">
											<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="<?php echo isset($this->customer['firstName']) ? $this->customer['firstName'] : ""; ?>">
										</div>
									</div>
									
									<div class="form-group row">
										<label for="lastName" class="col-sm-2 col-md-2 col-form-label text-right">Last Name</label>
										<div class="col-sm-10 col-md-10">
											<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="<?php echo isset($this->customer['lastName']) ? $this->customer['lastName'] : ""; ?>">
										</div>
									</div>
									
									
									<div class="form-group row">
										<label for="amount" class="col-sm-2 col-md-2 col-form-label text-right">
											Amount
										</label>
										<label for="amount" class="col-sm-10 col-md-10 col-form-label text-left">
											<strong> &#036;<?php echo isset($this->customer['amount']) ? $this->customer['amount'] : ""; ?></strong>
										</label>
									</div>
									
									<div class="form-group row">
										<label for="address1" class="col-sm-2 col-md-2 col-form-label text-right">
											Adress 1
										</label>
										<div class="col-sm-10 col-md-10">
											<input type="text" class="form-control" id="address1" name="address1" placeholder="Addres 1" value="<?php echo isset($this->customer['address1']) ? $this->customer['address1'] : ""; ?>">
										</div>
									</div>
									
									<div class="form-group row">
										<label for="address2" class="col-sm-2 col-md-2 col-form-label text-right">
											Adress 2
										</label>
										<div class="col-sm-10 col-md-10">
											<input type="text" class="form-control" id="address2" name="address2" placeholder="Addres 2" value="<?php echo isset($this->customer['address2']) ? $this->customer['address2'] : ""; ?>">
										</div>
									</div>
									
									<div class="form-group row">
										<label for="postCode" class="col-sm-2 col-md-2 col-form-label text-right">
											Postcode
										</label>
										<div class="col-sm-10 col-md-10">
											<input type="text" class="form-control" id="postCode" name="postCode" placeholder="PostCode" value="<?php echo isset($this->customer['postCode']) ? $this->customer['postCode'] : ""; ?>">
										</div>
									</div>
									
									<div class="form-group row">
										<label for="phone" class="col-sm-2 col-md-2 col-form-label text-right">
											Phone
										</label>
										<div class="col-sm-10 col-md-10">
											<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php echo isset($this->customer['phone']) ? $this->customer['phone'] : ""; ?>">
										</div>
									</div>
									
									
									
									<div class="form-group row">
										<div class="col-sm-10 col-md-12 mx-auto tex-center">
										  <button type="submit" class="btn btn-primary mx-auto text-center">Update</button>
										</div>
									  </div>
								</div><!--/.content-form-customer-->
								
								
					</div>
								<input type="hidden" name="userId" value="<?php echo $this->id;?>">  
							</form>
					</div>
				</div>
			</div>
		</div>
	
</div>

<!-- Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Method Transport</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Please select a Method Transport
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<script>
$('#formpay').on('submit', function(e){
	if ( !( $('input[name=transport]').is(':checked') ))
	{
		$('#alertModal').modal('show');
		e.preventDefault();
	}
});

$('#dataCustomer').submit( function(e){
	e.preventDefault();
	console.info("ese");
	$('.content-form-customer').hide();
	$('.lds-facebook').show();
	$.ajax({
		type: "POST",
		url: "upuser",
		dataType: 'json',
		data: $(this).serialize(),
		success: function(resp){
			console.info(resp)
			$('.content-form-customer').show();
			$('.lds-facebook').hide();
			$('#firstName').val(resp.firstName);
			$('#lastName').val(resp.lastName);
			$('#address1').val(resp.address1);
			$('#address2').val(resp.address2);
			$('#postCode').val(resp.postCode);
			$('#phone').val(resp.phone);
		},
		error: function(req){
			console.log('An error occurred.');
			console.log(data);
		}
	});
});
</script>
