<div class="col-md-6 mx-auto">
	<form class="form-signin" method="POST" action="dashboard">
		<h4 class="form-signin-heading text-center">
			<?php echo ( isset($this->title_message) ? $this->title_message : "" ); ?>
			Please Sign In
		</h4>
		<label for="inputEmail" class="sr-only">Email address</label>
		<input type="email" name="email" value="" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required value="">
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <?php
      if (  !empty(($this->msg)) )
      {
    ?>
        <div class="alert alert-danger m-4" role="alert">
          <?php echo !empty($this->msg) ? $this->msg : "" ?>
        </div> 
    <?php
      }
    ?>
     
		<?php
			foreach( $this->products as $key => $value )
			{
		?>
			<input type="hidden" name="<?php echo $key;?>" value="<?php echo $value; ?>" >
		<?php		
			}
		?>
	</form>
</div>
