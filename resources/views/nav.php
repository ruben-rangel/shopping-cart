<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal"><a href="/shopping-cart/public"><?php echo $this->title ?></a></h5>
	<nav class="my-2 my-md-0 mr-md-3">
		<a class="p-2 text-dark active" href="/shopping-cart/public">Products</a>
		<a class="p-2 text-dark link-cart" href="cart">Cart</a> 
		<span class="badge badge-dark link-cart">
			<a href="cart" id="link-cart" class="showNumP" style="color:#fff">
				<?php 
					echo (!empty($this->countCartProducts) ? $this->countCartProducts : "0") 
				?>
			</a> 
		</span>
		
		
		<?php
		if( isset( $this->email) )
		{
		?>
		<ul class="nav navbar-nav ">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					<?php echo $this->email ?> 
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li><a href="logout">Cerrar sesión</a></li>
				</ul>
			</li>
		</ul>
		<?php
		}else
		{
		?>
			<a class="btn btn-outline-primary" href="login">Sign up</a>
		<?php
		}
		?>
	</nav>
  
</div>