<div class="col-md-12">
	<h1 class="text-center mx-auto">Cart</h1>
</div>
<?php
$products = $this->products;
if ( count($products) === 0 )
{
?>
	<div class="col-md-12 text-center mx-auto m-5 alert alert-danger"><h2>You do not select product(s)</h2></div>
	<div class="col-md-12 text-center mx-auto" style>
		<a href="/shopping-cart/public"><i class="fas fa-home " style="color: #000;font-size:6rem;color: Dodgerblue;"></i></a>
	</div>
<?php
} else
{
?>
	<div class="col-md-12 text-center">
		<form action="checkout" class="p-5" method="POST">
			<div class="row">
				<?php
				$total = 0;
				foreach ($products as $product):
				$total+= $product['price'];
				?>
					<div class="col-md-12 text-left" id="row-<?php echo $product["id"]; ?>">
						<div class="row">
							<div class="col-md-7">
								<h4 style="display:inline;"><?php echo ucfirst($product['name']); ?></h4>
								<input style="display:inline;" type='number' name='quantity-<?php echo $product["id"]; ?>' value='1' min="1" class="text-right" data-id="<?php echo $product["id"]; ?>">
								<span style="display:inline;"><strong> x <?= number_format($product['price'], 2, '.', ',' ); ?></strong></span>
								
							</div>
							<div class="col-md-2" >
								<input data-delete="<?php echo $product["id"]; ?>" type="button" class="btn btn-danger float-left" value="Delete" style="display:inline;">
							</div>
							<div class="col-md-3 text-left row-<?php echo $product["id"]; ?>" style="display: flex;">
								&#36; <h5 id="scp<?php echo $product["id"]; ?>"><?php echo number_format( $product['price'], 2, '.', ',' ); ?></h5>
								<input data-priceid="<?php echo $product["id"]; ?>" type="hidden" value="<?php echo $product["price"]; ?>" name="price-<?php echo $product["id"]; ?>" id="price-<?php echo $product["id"]; ?>">
							</div>
							<div class="col-md-12">
								<p><?php echo $product["description"]; ?></p>
							</div>
						</div><!--/.row-->
					</div>
					<!--div class="col-md-4 text-left row-<?php echo $product["id"]; ?>" style="display: flex;">
						&#36; <h5 id="scp<?php echo $product["id"]; ?>"><?php echo number_format( $product['price'], 2, '.', ',' ); ?></h5>
						<input data-priceid="<?php echo $product["id"]; ?>" type="hidden" value="<?php echo $product["price"]; ?>" name="price-<?php echo $product["id"]; ?>" id="price-<?php echo $product["id"]; ?>">
					</div-->
				
				<div class="col-md-12"><hr></div>
				<?php endforeach; ?>
				<div class="col-md-8">&nbsp;</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12 text-right">
							<h4>Total <?php echo "(<span id='fstotal'>{$products->count()}</span>)"; ?> Items</h4>
						</div>	
						<div class="col-md-12 text-right" style="">
							<h4 style="display:inline;">&#36; </h4> <h3 id="total" style="display:inline;"> <?php echo number_format($total, 2, '.', ',' ); ?></h3>
						</div>
						<div class="col-md-12 text-right mt-2">
							<input type="submit" value="Proceed to Checkout" class="btn btn-info">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php
} //end else
?>
<script>
	$('input').on('change keyup', function(){
		let id_input = $(this).data('id');
		let price = $('#price-'+id_input).val();
		let sub_total = $(this).val() * price *1;
		$('#scp'+id_input).text(sub_total.toFixed(2));
		let total_p = 0;
		$( "h5[id^='scp']" ).each(function(){
			total_p += $(this).text() * 1;
		});
		$('#total').text(total_p.toFixed(2));
	});
	
	$('.btn-danger').on('click', function(){
		
		let pid = $(this).data('delete');
		console.info(pid);
		$('#row-'+pid).remove();
		$('.row-'+pid).remove();
		let total_p = 0;
		let n = 0;
		$( "h5[id^='scp']" ).each(function(){
			total_p += $(this).text() * 1;
			n += 1;
		});
		total_p = total_p;
		$('#total').text(total_p);
		$('#fstotal').text(n);
		if ( n == 0)
		{
			window.history.back();
		}
	});
</script>