<?php

namespace App\controllers;
use App\controllers\Config;
use App\Models\Product;

class ShoppingCart extends DB
{
  protected $product;
  protected $config;
  protected $title;

  public function __construct()
  {
	$this->product = new Product();
	$this->config = Config::get('site');
	$this->title		= $this->config["title"];
	$this->template = new Template(
		$this->config['view_path'] . "/layout.php",
		$this->config['view_path']. "/nav.php",
		$this->title
	);
	}
  
  protected function render($template, $data = array())
  {
    $this->config = Config::get('site');
    $this->template->render($this->config['view_path'] . "/layout.php", $data);
  }
}
