<?php
namespace App\controllers;

class Helpers
{
	public function arraykeyValue(Array $request_params, $name_input, $start = 0, $end )
	{
    //var_export($request_params);
		foreach( $request_params as $key => $value )
		{
			if ( substr($key, $start, $end ) == $name_input )
			{
				$aux[]= [$key => $value];
			}
		}
		// var_dump($aux);
		return $this->arraySimple($aux);
	}
	
	public function arraySimple(Array $s)
	{
		foreach($s as $key => $value )
		{
			foreach($value as $key1 => $value1){
				 $simple_array["$key1"] = $value1;
			}
		}
		return $simple_array;
	}	
	
	public function ids(Array $request_params)
	{
		/* Get array of ID's */
		foreach($request_params as $key => $value)
		{
			$id = explode("-", $key);
			$ids[] = $id[1];
			$ids = array_unique($ids);
		}
		return $ids;
	}
}//end helper
