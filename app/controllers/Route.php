<?php

namespace App\controllers;
use App\Models\{Product, User};
use App\controllers\ProductsController;
use App\controllers\PlaceOrder;
use App\controllers\LoginController;
use App\controllers\CheckOut;
use App\controllers\DashBoard;
use App\controllers\Session;

class Route
{
	public function isRouteValid()
	{
		global $Routes;
		//var_dump($Routes);
		$uri = self::validateRoute();
		if (!in_array(explode('?', $uri)[0], $Routes)) {
			return 0;
		} else {
			return 1;
    }
  }
  
  public static function validateRoute()
  {
		/* var_dump(
			$_SERVER['REQUEST_URI']." este es request uri",
			$_GET["url"]." get url"
		); */
		if (strpos($_SERVER['REQUEST_URI'], '/public') && $_GET['url'] === "index.php") {
			$uri = str_replace("/public", "", $_SERVER['REQUEST_URI']) . $_GET['url'];
		} else {
			$uri = str_replace("/public", "", $_SERVER['REQUEST_URI']);
		}
		return $uri;
  }

  // Insert the route into the $Routes array.
  private static function registerRoute($route)
  {
	  //var_dump($route." | route to register pegandole BASEDIR");
    global $Routes;
    $Routes[] = BASEDIR . $route;
	//print_r($Routes)." Esto es lo que esta en el arreglo y se le a colocado el BASEDIR ";
  }

  // This method creates dynamic routes.
  public static function dyn($dyn_routes)
  {
	  //var_dump($dyn_routes." entro dyn ... ");
    // Split the route on '/', i.e user/<1>
    $route_components = explode('/', $dyn_routes);
		//var_dump($route_components);
    // Split the URI on '/', i.e user/francis
    $uri_components = explode('/', substr($_SERVER['REQUEST_URI'], strlen(BASEDIR) - 1));

    // Loop through $route_components, this allows infinite dynamic parameters in the future.
    for ($i = 0; $i < count($route_components); $i++) {
      // Ensure we don't go out of range by enclosing in an if statement.
      if ($i + 1 <= count($uri_components) - 1) {
        // Replace every occurrence of <n> with a parameter.
        $route_components[$i] = str_replace("<$i>", $uri_components[$i + 1], $route_components[$i]);
      }
    }
    // Join the array back into a string.
    $route = implode("/", $route_components);
		//var_dump($route);
    // Return the route.
    return $route;
  }

  // Register the route and run the closure using __invoke().
  public static function set($route, $closure)
  {
	  
		
		
    if ($_SERVER['REQUEST_URI'] == BASEDIR . $route) {
		echo "ENTRA EN EL PRIMERO";
      self::registerRoute($route);
      $closure->__invoke();
    } /* else if (explode('?', $_SERVER['REQUEST_URI'])[0] == BASEDIR . $route) {
      self::registerRoute($route);
      $closure->__invoke();
    }  */else if ($_GET['url'] == explode('/', $route)[0]) {
		
      self::registerRoute(self::dyn($route));
      $closure->__invoke();
    }
  }

  /*
   * getRoute() is the method that actually checks if the current
   * route is valid or not.
  */
  public function getRoute()
  {
    global $Routes;
  
    $uri = $this->validateRoute();
		
    // Check if the route is in $Routes
    if (!in_array(explode('?', $uri)[0], $Routes)) {
      die('Invalid route.');
    }

    return $uri;
  }

  

  /*
   * If the route is valid create the view and the view controller.
   * If the route is invalid do nothing and if something goes wrong
   * checking the route return 0;
  */
  public static function make($view, Array $request_params)
  {
		$route = new Route();
		$controller = new ProductsController();
		if ( $route->isRouteValid() ) 
		{
			if ($view == 'layout')
			{
				$controller->listProducts();
			}
			if ($view == 'rating')
			{
				$controller->rating($request_params);
			}
			if ( $view == 'cart' )
			{
				$controller->cartList($request_params);
			}
			if ( $view == 'checkout' )
			{
				$checkOut = new CheckOut();
				$checkOut->checkOut($request_params);
			}
			if ( $view == 'placeorder' )
			{
				$session = new Session();
				$session->init(); 
				/*
				/* I take one of two paths:
				/*If you have a session, go to dashdoard.
				/*if you don't have a session, go to login 
				*/
				
				if ( $session->getStatus() === 2 && $session->get("email") )
				{
					$dash = new DashBoard();
					$dash->userDashBoard($request_params);
				} else {
					$login = new PlaceOrder();
					$login->formSignIn($request_params);
				}
					
			}
			if ( $view == 'login' )
			{
				$login = new LoginController();
				// $login->signIn($request_params);
				$login->checkEmail($request_params);
			}
			if ( $view == 'dashboard' )
			{
				$login = new LoginController();
				$login->checkEmail($request_params);
			}
			if ( $view == 'logout' )
			{
				$login = new LoginController();
				$login->logout();
			}
			if ( $view == 'payment' )
			{
				$payment = new DashBoard();
				$payment->payment($request_params);
				//var_dump($request_params);
			}
			
			if ( $view == 'upuser' )
			{
				$updateCustomer = new User();
				$updateCustomer->updateCustomer($request_params);
				//var_dump($request_params);
			}
			
			
			return 1;
		}
  }

  /*
   * The run() method is the first method that runs.
   * run() gets the current route and checks if it is valid.
   * If the route is invalid the app doesn't proceed any further.
  */
  public function run()
  {
    // Should be capturing the output of this method. We will at some point.
    $this->getRoute();
  }
}
