<?php
namespace App\controllers;

/* 
/*  Class to build the main view and load the new sections 
/*  depending on the client's requests 
*/

class Template {
		protected $base_template;
		protected $page;
		protected $nav;
		protected $title;
		
		public function __construct($base_template,$nav, $title)
		{
			$this->title = $title;
			$this->nav = $nav;
			$this->base_template = $base_template;
		}

		public function render($page, $data = Array())
		{
			foreach ( $data as $key => $value ) 
			{
				$this->{$key} = $value;
				
			}
			require $this->nav;
			$this->page = $page;
			require $this->base_template;
		}
    
		public function content()
		{
		  require $this->page;
		}
}
