<?php
namespace App\controllers;
use App\controllers\Session;
use App\controllers\Template;
use App\Models\Product;

class CheckOut extends ShoppingCart
{
	private $session;
	protected $product;
	protected $template;
	protected $config;
	protected $title;
	protected $email;
	
	public function __construct()
	{
		$this->session = new Session();
		$this->session->init();

		if( !empty ( $this->session->get("email") ) )
			$this->email = $this->session->get("email");
				

		
		$this->product = new Product();
		//$this->email = $this->session->get("email");
		$this->config   = Config::get('site');
		$this->title	= $this->config["title"];
		$this->template = new Template(
			$this->config['view_path']."/layout.php",
			$this->config['view_path']."/nav.php",
			$this->title
		);
	}
	
	/* Proced to CheckOut */
  public function checkOut(Array $productsCheckOut)
  {
	  //var_dump($productsCheckOut, $this->session->getStatus());
	  /* Get Only Quantity via name input */
		$arrKeyQuantity = array_map( 
			function ($key, $value) 
			{
				if (substr($key, 0, 9) == 'quantity-')
				{
					return [ $key => $value];
				}
			}, array_keys($productsCheckOut), $productsCheckOut
		);
		//var_dump($arrKeyQuantity);
		/* Delete NULL values */
		$arrKeyQuantity = array_values(array_filter($arrKeyQuantity));
		
		/* Obtain array unidimensional pair key => value*/
		foreach($arrKeyQuantity as $key => $value )
		{
			foreach($value as $key1 => $value1){
				$arrayKeyCuantity["$key1"] = $value1;
			}
    }
    //var_dump($arrKeyQuantity);
		/* Get array of ID's */
		foreach($productsCheckOut as $key => $value)
		{
			$id = explode("-", $key);
			$ids[] = @$id[1];
			$ids = array_unique($ids);
		}
		$productsTotal = array_chunk($productsCheckOut,2);
		//var_dump($productsTotal);
		
		/* Get individual sub Total per product */
		foreach( $productsTotal as $subt){
			$sub_total[] = $subt[0] * $subt[1];
		}
		
		/* Get total amount of products in cart */
		$total = array_sum($sub_total);
		
		$products = $this->product->getEspecificProducts($ids);
		$countCartProducts = count($products);
		$this->template->render(
			$this->config['view_path'] . 
			"/checkout.php", 
			[
				"products" => $products,
				"total" => $total,
				"countCartProducts" => $countCartProducts,
				"arrayKeyCuantity" => $arrayKeyCuantity,
				"email"	=> $this->email,
				"auxData" => $productsCheckOut,
			]);
  }
}
