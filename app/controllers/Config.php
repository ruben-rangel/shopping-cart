<?php

namespace App\controllers;

class Config
{
  static public $directory;
  static public $config = [];

  static public function setDirectory($path)
  {
    self::$directory = $path;
  }

  static public function get($config)
  {
		$config = strtolower($config);
		// self::$config[$config] = require self::$directory . '/' . $config . '.php';
		self::$config[$config] = require "../config/" . $config . ".php";

		return self::$config[$config];
  }
}
