<?php

namespace App\controllers;
use App\Models\Product;
use App\Models\Rating;
use App\controllers\Session;
use App\controllers\Template;

class ProductsController extends ShoppingCart
{
	
	protected $session;
	protected $product;
	protected $template;
	protected $config;
	protected $title;
	protected $email;
	
	public function __construct()
	{
		$this->product 		= new Product();
		$this->session 	= new Session();
		$this->config   = Config::get('site');
		$this->title	= $this->config["title"];
		
		$this->template = new Template(
			$this->config['view_path']."/layout.php",
			$this->config['view_path']."/nav.php",
			$this->title
		);
	}
	
	/* List all products in Index Page */
  public function listProducts()
  {
	  $this->session->init();
	  
		if( !empty ( $this->session->get("email") ) ){
			$this->email = $this->session->get("email");
			$session_id = $this->session->get("session_id");
		}
		
		$products = Product::select( 
			"products.id",
			"products.name",
			"products.description",
			"products.image",
			"products.price")
			->selectRaw( 'ROUND(AVG( rating ),0) as rating' )
			->leftJoin("ratings", "products.id", "=", "ratings.product_id")
			->groupBy("products.id")
			->get(); 

		$this->template->render(
			$this->config['view_path'] . "/products.php", 
			[
				"products" => $products,
				"email"	=> $this->email,
				"session_id"	=> !empty($session_id) ? $session_id : "",
			] 
		);
  }

  /* Go to  List Product(s) in Cart */
  public function cartList(Array $productsSelected)
  {
		
		$this->session->init();
		
		if( !empty ( $this->session->get("email") ) )
			$this->email = $this->session->get("email");

		if ( !empty($productsSelected) )
		{
			$products = array_diff(str_split($productsSelected["prodSel"]),[","]);
			$products = $this->product->getEspecificProducts($products);
			$countCartProducts = count($products);
			$this->template->render(
				$this->config['view_path'] . "/cart-products.php", 
				[
					"products" => $products,
					"countCartProducts" => $countCartProducts,
					"email"	=> $this->email,
				]
			);
		} else {
			$this->template->render(
			$this->config['view_path'] . "/cart-products.php", 
			[
				"products" => [],
				"countCartProducts" => 0
			]
		);
	}
  }
  
  public function rating($request_params)
  {
	  /* Session */
	  $this->session->init();
	  
	  if ( !empty($this->session->get("session_id")) )
	  {
			$rating = [
				"msg"=>"Only One vote per Session. Thanks!",
				"flag"=> 0,
				
			];
		  echo json_encode( $rating );
		  exit;
	  }
	  $this->session->add( "session_id",md5($this->session->get("email")) );
	  
	  /* ratings */
	  $rating = new Rating();
	  $rating->rating = $_POST["rating"];
	  
	  /* Search Specific Product to Rating*/
	  $product = $this->product->find($_POST["product_id"]);
	  
	  /*Save product*/
	  $product->ratings()->save($rating);
	  
	  /* Data to layaout products | response ajax */
	  $rating = round( $product->ratings()->avg('rating') );
	  $rating = [
		"rating"=>$rating,
		"product_id"=>$_POST["product_id"],
		"session_id"=>$this->session->get("session_id"),
		"msg" => "Thanks for you rate.",
		"flag" => 1,
		];
	  //$session_id = $this->session->get("id_session");
	  echo json_encode($rating);
	  
  }
}
