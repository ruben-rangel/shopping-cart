<?php
namespace App\controllers;
use App\controllers\Session;
use App\controllers\Template;
use App\controllers\Helpers;
use App\controllers\DashBoard;
// use App\controllers\DB;
use App\Models\User;
use App\Models\Customer;

/**
* Login controller
*/
class LoginController extends ShoppingCart
{
	private  $session;
	protected $config;
	protected $title;
	protected $template;
	protected $user;
	protected $email;
	protected $dataCustomer;
	protected $id;
	protected $helpers;
	
	public function __construct()
	{
		$this->user 		= new User();
		$this->session 	= new Session();
		$this->config   = Config::get("site");
		$this->title		= $this->config["title"];
		$this->helpers = new Helpers();
		$this->dataCustomer = new Customer();
		$this->template = new Template(
		$this->config['view_path']."/layout.php",
		$this->config['view_path']."/nav.php",
		$this->title
		);
  }
  
  /* Check Email Exist or Not */
  public function checkEmail($request_params){
    if (isset($request_params["email"])) 
    {
      $this->email	= isset($email_storage->email) ? $email_storage->email : $request_params["email"] ;  
      
      $email_storage = $this->user->getEspecificUsersEmail($request_params['email']);
      if ( is_null($email_storage) )
        $this->registerEmail(
          $this->email ,
          $request_params["password"], 
				$request_params);
      else
        $this->checkPassword( $request_params, $email_storage );
    } else { //login via url
      $this->login("","");
    }
  }

  /* Check Password */
  private function checkPassword($request_params, /*Object*/ $email)
  {
	  /* Init Session */
	  $this->session->init();
	  $this->session->add("email", $request_params["email"]);
	  
	  
	if ( isset($request_params["total"]) && isset($request_params["password"]) )
    {
      $this->id = $email->id;
      if ( !password_verify( $request_params["password"], $email->password ) )
      {
        $this->template->render(
          $this->config['view_path'] . "/login.php", 
          [
            "msg" => "User or password Fail !",
            "products" => [],
            "id" => $this->id,
          ] 
        ); 
      }
	  
      $helpers = new Helpers();
	  
	  /* Data */
      $array_names = $helpers->arraykeyValue($request_params,"name", 0, 4);
      $array_quantity = $helpers->arraykeyValue($request_params,"quantity-", 0, 9);
      $array_price = $helpers->arraykeyValue($request_params,"price-", 0, 6);
      $ids = $helpers->ids($array_names); 
      $countCartProducts = count($ids);

		/*User Amount*/
		$this->dataCustomer = $this->user->find($this->id)->customer;

		/* Get all Order to this User */
		$ordersUser = $this->user->find($this->id)->orders;
		
		 $this->template->render(
        $this->config['view_path'] . "/dashboard.php", 
        [
          "auxData" => $request_params,
          "email"		=> $this->session->get("email"),
          "ids"     => $ids,
          "names"   => $array_names,
          "quantity"   => $array_quantity,
          "prices"   => $array_price,
          "countCartProducts"   => $countCartProducts,
          "products"  => $request_params,
          "id"	=> $this->id,
          "customer"	=> $this->dataCustomer,
          "title_message"	=> "Welcome",
          "ordersUser"	=> $ordersUser,
        ] 
      );
    } else {  //No products on cart
		/* No match Passwords */
	   if ( !password_verify( $request_params["password"], $email->password ) )
      {   //Return come back to Login
        return $this->login( $request_params["email"], "User or password Fail !");
      }  else {   //Go to Dasboard
        $this->toDashBoard($email);
      }
    }
  }

  private function registerEmail($email, $password, $request_params )
  {
	$this->user->email = ($this->email);
	$this->user->password = password_hash($password,PASSWORD_DEFAULT);
   $this->user->save();
	
	$this->dataCustomer->amount = 100;
	$this->user->customer()->save($this->dataCustomer);
	
		/* If user selected products in cart */
		if ( count($request_params) > 2 )
		{
			$array_prices = $this->helpers->arraykeyValue($request_params,"price-", 0, 6);
			$array_names = $this->helpers->arraykeyValue($request_params,"name-", 0, 5);
			$array_quantity = $this->helpers->arraykeyValue($request_params,"quantity-", 0, 9);
			$ids = $this->helpers->ids($array_prices);
			$countCartProducts = count($array_quantity);
		}
	
		$this->session->init();
		$this->session->add('email', $email);
		$this->template->render(
      $this->config['view_path'] . "/dashboard.php", 
      [
        "auxData" 		=> $request_params,
        "email"		  	=> $this->user->email,
        "id"	      	=> $this->user->id,
        "products"		=> $request_params,
        "names"			=> !empty($array_names) ? $array_names : "",
        "prices"			=> !empty($array_prices) ? $array_prices : "",
        "quantity"		=> !empty($array_quantity) ? $array_quantity : "",
        "ids"				=> isset($ids) ? $ids : "",
        "customer"			=> $this->dataCustomer,
        "countCartProducts"	=> !empty($countCartProducts) ? $countCartProducts : 0,
      ] 
    );
  }
  
  public function toDashBoard( $email )
  {
	  if ( !$this->session->get("email") )
	  {
			$this->session->init();
			$this->session->add("email", $email->email);
	  }
	  
		$customer = $this->user->find($email->id)->customer;
		
		/*Get All Orders by User*/
		$ordersUser = $this->user->find($email->id)->orders;
		
		/* Customer */
		$customer = $this->user->find($email->id)->customer;
		
    $this->template->render(
    $this->config['view_path'] . "/dashboard.php", 
      [
        "products" 	=> [],
        "email"	=> $this->email,
        "id"	=> $email->id,
        "title_message" => "Welcome",
        "ordersUser" => !empty($ordersUser) ? $ordersUser : 0,
        "customer" => $customer,
      ] 
    );
  }
	
	public function login( String $request_params, String $msg )
	{
		$this->template->render(
		$this->config['view_path'] . "/login.php", 
			[
        "products" 	=> [],
        "msg"	=> $msg,
			] 
		);
	}

  public function logout()
	{
		$this->session->init();
		$this->session->close();
		$this->template->render(
			$this->config['view_path'] . "/login.php", 
				[
					"products" 	=> [],
				] 
			);
	}
}
