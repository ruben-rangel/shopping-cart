<?php
namespace App\controllers;
/**
 * Class Administrative Sessions
 */
class Session
{
  /**
   * Init Session
   */
  public function init()
  {
		session_start();
  }

  /**
   * Add Elements to Array Session
   */
  public function add($key, $value)
  {
    $_SESSION[$key] = $value;
  }

  /**
   * Returns value element Session
   */
  public function get($key)
  {
    return !empty($_SESSION[$key]) ? $_SESSION[$key] : null;
  }

  /**
   * Return all values array session
   */
  public function getAll()
  {
    return $_SESSION;
  }

  /**
   * Remove elements session
   */
  public function remove($key)
  {
    if(!empty($_SESSION[$key]))
      unset($_SESSION[$key]);
  }

  /**
   * Close Session
   */
  public function close()
  {
    session_unset();
    session_destroy();
  }

  /**
   * Get status session
   */
  public function getStatus()
  {
    return session_status();
  }
}
