<?php

namespace App\controllers;
use App\controllers\Config;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Model;

/* This class is responsible for making the connection 
to the database. It is instantiated from the models */

class DB extends Model
{
  /** @var \Illuminate\Database\Capsule\Manager */
  protected $capsule;

  /** @var App\DB */
  protected static $instance;

  /**
   * @return App\DB
   */
  public function __construct()
  {
    $this->_makeEloquent(new Capsule());
    return $this;
  }

  /**
   * Get instance of current class
   * @return App\DB
   */
  public static function getInstance()
  {
    if (!static::$instance instanceof DB) {
      static::$instance = new DB();
    }
    return static::$instance;
  }

  /**
   * Get instance of capsule
   * @return \Illuminate\Database\Capsule\Manager
   */
  public function capsule()
  {
    return $this->capsule;
  }

  /**
   * Setup eloquent database
   * @param \Illuminate\Database\Capsule\Manager $capsule
   * @return App\DB
   */
  private function _makeEloquent(Capsule $capsule)
  {
    //Config::setDirectory("../config");
    //Get parameters connection from config file with the Controller Config
    $config = Config::get("database");
    //var_dump($config);
    // Get capsule instance
    $this->capsule = $capsule;

    // Setup connection defaults
    $configs = array(
      "driver"    => $config["driver"],
      "host"      => $config["host"],
      "database"  => $config["database"],
      "username"  => $config["username"],
      "password"  => $config["password"],
      "charset"   => $config["charset"],
      "collation" => $config["collation"],
      "prefix"    => $config["prefix"],
    );

    // Setup connection
    $this->capsule->addConnection($configs);
    
    // Set as global
    $this->capsule->setAsGlobal();

    // Boot eloquent
    $this->capsule->bootEloquent();
    return $this;
  }
}
