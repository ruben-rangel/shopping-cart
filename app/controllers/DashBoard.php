<?php

namespace App\controllers;
use App\controllers\Session;
use App\controllers\LoginController;
use App\Models\User;
use App\Models\Order;
use App\Models\Customer;

class DashBoard
{
	protected $session;
	protected $config;
	protected $title;
	protected $template;
	protected $user;
	protected $email;
	protected $idUser;
	protected $dataOrders;
	protected $dataCustomer;
  
	
	public function __construct()
	{
		
		$this->session  = new Session();
		if ( session_status() != 2 ) {
			
			$this->session->init();
		}
		
		$this->email 	  = $this->session->get("email");
		$this->config   = Config::get("site");
		$this->title		= $this->config["title"];
		$this->user 	  = new User();
		$this->dataCustomer = new Customer();
		$this->dataOrders = new Order();
		$this->template = new Template(
		$this->config['view_path']."/layout.php",
		$this->config['view_path']."/nav.php",
		$this->title
		);
	}
	
	public function userDashBoard($request_params)
  {
		$email_storage = $this->user->getEspecificUsersEmail($this->email);
		$idUser = $email_storage->id;
		
		$helpers = new Helpers();
		
		$array_names = $helpers->arraykeyValue($request_params,"name", 0, 4);
		$array_quantity = $helpers->arraykeyValue($request_params,"quantity-", 0, 9);
		$array_price = $helpers->arraykeyValue($request_params,"price-", 0, 6);
		$count = count($array_price);
		$ids = $helpers->ids($array_price);
		$amountUser = $this->user->getAmountUser($idUser, 0);
		
		/*Get All Orders of User*/
	  $ordersUser = $this->user->find($idUser)->orders;
	  
	  /* Get Data User -> Customer */
	  $this->dataCustomer = $this->user->find($idUser)->customer;
	  //var_dump($array_names,$array_quantity,$array_price,$ids);
	  $this->template->render(
			$this->config['view_path'] . "/dashboard.php", 
			[
				"title_message" => "Welcome ",
				"products"		  => $request_params,
				"email"			    => $this->email,
				"title"			    => $this->title,
				"auxData"			  		=> $request_params,
				"ids"				    => $ids,
				"names"			    => $array_names,
				"quantity"		  => $array_quantity,
				"prices"			  => $array_price,
				"id"            => $email_storage->id,
				"countCartProducts" => $count,
				"customer" => $this->dataCustomer,
				"ordersUser" => $ordersUser,
			]
		);
  }
  
  /* Method to proced Orders */
	public function payment($request_params)
	{
		$this->email = $this->session->get("email");
		
		foreach( $request_params["id"] as $key => $value  )
		{
			foreach($request_params as $key1 => $value1){
				
				$get_id = explode("-",$key1);
				$id = @$get_id[1];
				
				if ( $value == $id )
				{
					${"product_".$value}[] = $value1;
					
				}
			}	//end foreach
			
			/* Save data in Tables */
		  $this->dataOrders->user_id = $request_params["idUser"];
		  $this->dataOrders->iddelivery = $request_params["transport"];
		  $this->dataOrders->status = 1;
		  $this->dataOrders->total = $request_params["total"];
		  $this->dataOrders->save();
		  
		  $this->dataOrders->products()->attach(
			$value,
			[
			  "price"  => ${"product_".$value}[1],
			  "quantity"  => ${"product_".$value}[2],
			]
		  );
		} //endforeach
    
		
		  /* Amount */
		  $this->idUser = $request_params["idUser"];
		  
			$userAmount = $this->user->getAmountUser($this->idUser, $request_params["total"]);

			/* Get all Orders*/
		  $ordersUser = $this->user->find($this->idUser)->orders;
		  
			/* Get all data User -> Customer */
			$this->dataCustomer = $this->user->find($this->idUser)->customer;
		  
			$this->template->render(
			$this->config['view_path'] . "/dashboard.php", 
			[
				"title_message" => "Your order has been successfully processed ",
				"products"		=> [],
				"email"			=> $this->email,
				"title"			=> $this->title,
				"id"				=> $request_params["idUser"],
				"customer"		=> $this->dataCustomer,
				"ordersUser"		=> $ordersUser,
			]
			);
			
	}
}
