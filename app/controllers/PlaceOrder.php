<?php
namespace App\controllers;
use App\controllers\Session;
use App\controllers\ShoppingCart;
use App\controllers\Helpers;

class PlaceOrder /* extends ShoppingCart */
{
	private $session;
	protected $config;
	private $template;
	protected $email;
	protected $title;
	
	public function __construct()
	{
		$this->session 	= new Session();
		$this->config = Config::get('site');
		$this->title	= $this->config["title"];
		$this->template = new Template(
			$this->config['view_path'] . "/layout.php",
			$this->config['view_path']. "/nav.php",
			$this->title
		);
	} 
	
	public function formSignIn($request_params)
  {
		/* $this->session->init();
		/*if email exits User is Logged*/
		/*if ( $this->session->getStatus() === 1 ||
			empty( $this->session->get("email") )
		)
			$this->session->close();
		else
			$this->email = $this->session->get("email"); */

		$helpers = new Helpers();
		$array_names = $helpers->arraykeyValue($request_params, "name-", 0, 5);
		$countCartProducts = count($array_names);

		$this->template->render(
			$this->config['view_path'] . "/login.php", 
			[
				"title_message" => "You are not registered.",
				"products"		=> $request_params,
				"email"			=> $this->email,
				"title"			=> $this->title,
				"countCartProducts"	=> $countCartProducts,
				"dataAux"	=> $request_params,
				
			]
		);
  }
  
}
