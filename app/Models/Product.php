<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\controllers\DB;

class Product extends DB
{
  public function orders()
    {
        return $this->belongsToMany('App\Models\Product')
        ->withPivot('quantity', 'price')
        ->withTimestamps();
    }
	
	public function ratings()
	{
		return $this->hasMany("App\Models\Rating");
	}
	
	public function avgRatings()
	{
		return $b = DB::join("ratings", "ratings.product_id", "=", "products.id")
			->select( "ratings.product_id"  )
			->groupBy("ratings.product_id")
			->get();
	}

  public function getAllProducts()
  {
    /* all() is a method of Model */ 
    return self::all();
  }
  /* Search by IDs */
  public function getEspecificProducts(Array $selected)
  {
	  return self::find($selected);
  }
}
