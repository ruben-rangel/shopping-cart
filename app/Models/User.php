<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\controllers\DB;

class User extends DB
{
	/*Relation one to One with customer*/
	public function customer()
	{
		return $this->hasOne('App\Models\Customer','user_id', 'id');
	}
	
	/*Relation one to Many with Orders*/
	public function orders()
	{
		return $this->hasMany('App\Models\Order');
	}
	
	
  public function getAllUsers()
  {
    /* all() is a method of Model */ 
    return self::all();
  }
  /* Search by IDs */
  public function getEspecificUsersEmail(String $email)
  {
	  return self::where('email',$email)->first();
  }

	public function getAmountUser($idUser, $total = 0)
	{
		/* Amount */
		  $user = self::find($idUser); 
		  $userAmount = self::find($idUser)->customer;
		  $userAmount->amount -= $total;
		  $user->customer()->save($userAmount);
		return $userAmount->amount;
	}
	
	public function updateCustomer($request)
	{
		//print_r($request);
		$customer = $this->find($request["userId"])->customer;
		$customer->firstName = $request["firstName"];
		$customer->lastName = $request["lastName"];
		$customer->address1 = $request["address1"];
		$customer->address2 = $request["address2"];
		$customer->postCode = $request["postCode"];
		$customer->phone = $request["phone"];
		$customer->save();
		echo json_encode($customer);
	}
}
