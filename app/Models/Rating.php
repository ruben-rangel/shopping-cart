<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\controllers\DB;

class Rating extends DB
{
	public function product()
	{
		return $this->belongsTo("App\Models\Product");
	}
}