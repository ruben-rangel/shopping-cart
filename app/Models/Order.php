<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\controllers\DB;

class Order extends DB
{
	/*Relation one to Many with Products*/
	public function products()
	{
    return $this->belongsToMany('App\Models\Product')
    ->withPivot("quantity", "price")
    ->withTimestamps();
	}
	
  /*Relation one to Many with User*/
	public function user()
	{
    return $this->belongsTo('App\Models\User');
	}
}