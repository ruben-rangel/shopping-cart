<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\controllers\DB;

class Customer extends DB
{
	protected $fillable = ['amount'];
	public function user()
	{
		return $this->belongsTo("App\Models\User",'user_id', 'id');
	}
}

